// import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard'

import { useState, useEffect } from 'react';

export default function Courses(){

	const [courses, setCourses] = useState([])

/*	const courses = coursesData.map(course => {
		return (
			<CourseCard key={course.id} course = {course}/>
		)
	})*/

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setCourses(data.map(course => {
				<CourseCard key={course.id} course = {course}/>
			}))
		})
	}, [])

	return (
		<>
		{courses}
		</>
	)
}